
FROM archlinux:latest

MAINTAINER Jenkin Schibel <jenkin.schibel@protonmail.com> 

RUN locale-gen --lang "en_US.UTF-8"
RUN pacman -Syu --noconfirm
RUN pacman -S  --noconfirm python python-pip openssh git mercurial npm 

RUN pip install kallithea gearbox

RUN mkdir /kallithea/
RUN mkdir /kallithea/config/
RUN mkdir /kallithea/repos/
RUN mkdir /kallithea/logs/

RUN kallithea-cli front-end-build

ADD run.sh /kallithea/run.sh
RUN chmod 755 /kallithea/run.sh

EXPOSE 5000

CMD "/kallithea/run.sh"
