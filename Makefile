

run:
	docker-compose up
image:
	docker build . -t dukeofcool199/kallithea
clean:
	docker rm kallithea
	docker rmi $(docker images -f "dangling=true" -q) 
	docker rmi dukeofcool199/kallithea
