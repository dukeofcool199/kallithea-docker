cd /kallithea/

if [ ! -d /kallithea/ ]; then
    echo "creating root directory"
    mkdir kallithea; 
fi

if [ ! -d /kallithea/config/ ]; then
    echo "creating config directory"
    mkdir /kallithea/config/;
fi

if [ ! -d /kallithea/repos/ ]; then 
    echo "creating repos directory"
    mkdir /kallithea/repos/;
fi

if [ ! -d /kallithea/logs ]; then
    echo "creating logs directory"
    mkdir /kallithea/logs/; 
fi

if [ ! -f /kallithea/config/my.ini ]; then
    echo "creating base configuration file: my.ini"
    kallithea-cli config-create /kallithea/config/my.ini host=0.0.0.0;
fi


if [ ! "$(ls -A /kallithea/repos/)" ]; then
    echo "initializing database"
    kallithea-cli db-create -c /kallithea/config/my.ini --user=$ADMIN_USER --password=$ADMIN_PASS --email=$ADMIN_EMAIL --repos=/kallithea/repos/ --force-yes;

fi 

gearbox serve -c /kallithea/config/my.ini;
